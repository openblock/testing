## 说明

本目录用以存放其他分类的示例项目，包含一些基础教程。


收录项目：
| 项目名称 | 描述 |
| :--- | :--- |
| [helloworld](./helloworld/) | 你好，世界！ |
| [一扇门](./onedoor/) | 一扇门 |
| [两扇门](./twodoor/) | 两扇门 |
| [六扇门](./sixdoor/) | 六扇门 |
| [输入（提示询问）](./input/) | 输入 |
| [生成CSV文件](./csvtest/) | 测试生成CSV文件 |
| [码位](./ascii/) | 输出对应acsii码 |
| [字符串扩展](./stringtest/) | 使用字符串 |
| [消息只读性测试](./readonlytest/) | 消息组件测试 |
| [窗口操作](./window/) | 浏览器窗口 |
| [询问和回答](./q&a/) | 你问我答 |
| [文件操作测试](./filetest/) | 测试文件操作 |
| [使用列表](./listtest/) | 测试使用列表数据 |
| [输入框](./input-component/) | 输入框 |
| [保存和恢复状态](./save-recover/) | 保存和恢复状态 |
| [门的状态切换](./fsm-door/) | 门的状态切换 |
| [文本连接](./text-join/) | 文本拼接 |
| [canvas坐标](./canvas-layout/) | 获取canvas坐标 |
| [函数样例](./function/) | 函数调用样例 |
| [函数样例二](./function2/) | 函数调用样例2 |
| [延时消息](./delayprint/) | 延时输出打印 |
| [切换状态传参](./state-switching/) | 状态转换传参操作 |
| [状态切换](./state-change/) | 状态切换 |
| [画板宽高](./canvas-size/) | 获取画板的大小 |
| [发现FSM](./findfsm/) | 发现状态机 |
| [条件语句](./condition/) | 条件语句示例 |
| [事件状态转换](./event/) | 发生事件导致的转换 |
| [文本合成](./text-creator/) | 输出教程 |