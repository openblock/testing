## OpenBlock 狮偶示例仓库

- 在线地址：https://mlzone.areyeshot.com/testing/

## 🚀目录结构

- sample：示例项目存放目录，具体细节请参考sample目录下的`README.md`
- static：静态资源目录，存放示例项目网站所需的静态资源
- index.html：示例项目网站首页
- list.json：示例项目列表配置文件
- license：开源协议
- `README.md`：狮偶示例仓库的`README`文件

## 😀做出贡献

欢迎提交PR，提交PR前请先阅读狮偶社区的[贡献指南](https://fcn73mb3nzl0.feishu.cn/wiki/Po5hwXVSLi9PfpkeSyEchFdFn4K?from=from_copylink)